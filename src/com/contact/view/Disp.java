package com.contact.view;

public class Disp {

    public static void print(String statement) {
        System.out.println(statement);
    }

    public static void printLine(String statement) {
        System.out.print(statement);
    }

}
