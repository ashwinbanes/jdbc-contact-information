package com.contact.view;

import com.contact.dao.ContactDao;
import com.contact.delegates.Delegate;
import com.contact.model.Contact;
import com.contact.util.Phone_Type;
import com.contact.util.PrintStatements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class View {

    public static void main(String[] args) {
        Phone_Type phone_type = new Phone_Type();
        phone_type.getPhoneType();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            Disp.print(PrintStatements.CONTACT_CRUD);
            String option = "";
            String line = "";
            while (true) {
                Disp.print(PrintStatements.CONTINUE_EXIT_MENU);
                line =  br.readLine();
                if (line.equals("No")) break;
                Disp.print(PrintStatements.MENU);
                option = br.readLine();
                switch (option) {
                    case "1":
                        addContact();
                        break;
                    case "2":
                        updateContact();
                        break;
                    case "3":
                        viewContact();
                        break;
                }
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void viewContact() {

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            Disp.print(PrintStatements.FIRST_NAME);
            String first_name = br.readLine();
            Disp.print(PrintStatements.LAST_NAME);
            String last_name = br.readLine();
            Contact contact = new Contact();
            contact.setFirstName(Delegate.addQuotes(first_name));
            contact.setLastName(Delegate.addQuotes(last_name));
            ContactDao contactDao = new ContactDao();
            contactDao.viewContact(contact);
        }catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void updateContact() {

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            Disp.print(PrintStatements.FIRST_NAME);
            String first_name = br.readLine();
            Disp.print(PrintStatements.LAST_NAME);
            String last_name = br.readLine();
            Contact contact = new Contact();
            contact.setFirstName(Delegate.addQuotes(first_name));
            contact.setLastName(Delegate.addQuotes(last_name));
            Disp.print(contact.getFirstName());
            Disp.print(contact.getLastName());
            Disp.print(PrintStatements.PHONE_NUMBER);
            Disp.print(PrintStatements.NUMBER_FORMAT_HELPER);
            Disp.print(Delegate.OFFICE
                    + PrintStatements.SPACE + PrintStatements.NUMBER_FORMAT_OFFICE);
            Disp.print(Delegate.MOBILE
                    + PrintStatements.SPACE + PrintStatements.NUMBER_FORMAT_MOBILE);
            Disp.print(Delegate.HOME
                    + PrintStatements.SPACE + PrintStatements.NUMBER_FORMAT_HOME);
            String number = br.readLine();
            while (!Delegate.numberValidator(number)) {
                Disp.print(PrintStatements.VALID_PHONE_NUMBER);
                number = br.readLine();
            }
            String[] numberSplit = number.split("[-]");
            ContactDao contactDao = new ContactDao();
            contact.setFirstName(first_name);
            contact.setLastName(last_name);
            String countryCode, areaCode, no, extension;
            if (Delegate.TYPE_SPECIFIER.equals(Delegate.OFFICE)) {
                no = numberSplit[0];
                String[] noSplit = no.split("[x]");
                extension = noSplit[1];
                no = noSplit[0];
                contact.setNumber(no);
                contact.setOfficeExtension(extension);
                contactDao.updateContact(contact, Delegate.OFFICE);
            }
            if (Delegate.TYPE_SPECIFIER.equals(Delegate.MOBILE)) {
                countryCode = numberSplit[0];
                no = numberSplit[1];
                contact.setNumber(no);
                contact.setCountryCode(countryCode);
                contactDao.updateContact(contact, Delegate.MOBILE);
            }
            if (Delegate.TYPE_SPECIFIER.equals(Delegate.HOME)) {
                countryCode = numberSplit[0];
                areaCode = numberSplit[1];
                no = numberSplit[2];
                String[] noSplit = no.split("[x]");
                no = noSplit[0];
                extension = noSplit[1];
                contact.setCountryCode(countryCode);
                contact.setAreaCode(areaCode);
                contact.setNumber(no);
                contact.setHomeExtension(extension);
                contactDao.updateContact(contact, Delegate.HOME);
            }
        }catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void addContact() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            Disp.print(PrintStatements.FIRST_NAME);
            String first_name = br.readLine();
            Disp.print(PrintStatements.LAST_NAME);
            String last_name = br.readLine();
            Disp.print(PrintStatements.ENTER_EMAIL_ADDRESS);
            String email = br.readLine();
            while (!Delegate.emailValidator(email)) {
                Disp.print(PrintStatements.VALID_EMAIL_ADDRESS);
                email = br.readLine();
            }
            Disp.print(PrintStatements.PHONE_NUMBER);
            Disp.print(PrintStatements.NUMBER_FORMAT_HELPER);
            Disp.print(PrintStatements.PHONE_NUMBER);
            Disp.print(PrintStatements.NUMBER_FORMAT_HELPER);
            Disp.print(Delegate.OFFICE
                    + PrintStatements.SPACE + PrintStatements.NUMBER_FORMAT_OFFICE);
            Disp.print(Delegate.MOBILE
                    + PrintStatements.SPACE + PrintStatements.NUMBER_FORMAT_MOBILE);
            Disp.print(Delegate.HOME
                    + PrintStatements.SPACE + PrintStatements.NUMBER_FORMAT_HOME);
            String number = br.readLine();
            while (!Delegate.numberValidator(number)) {
                Disp.print(PrintStatements.VALID_PHONE_NUMBER);
                number = br.readLine();
            }
            String[] numberSplit = number.split("[-]");
            Contact contact = new Contact();
            ContactDao contactDao = new ContactDao();
            contact.setFirstName(first_name);
            contact.setLastName(last_name);
            contact.setEmail(email);
            String countryCode, areaCode, no, extension;
            if (Delegate.TYPE_SPECIFIER.equals(Delegate.OFFICE)) {
                no = numberSplit[0];
                String[] noSplit = no.split("[x]");
                extension = noSplit[1];
                no = noSplit[0];
                contact.setNumber(no);
                contact.setOfficeExtension(extension);
                contactDao.addContact(contact, Delegate.OFFICE);
            }
            if (Delegate.TYPE_SPECIFIER.equals(Delegate.MOBILE)) {
                countryCode = numberSplit[0];
                no = numberSplit[1];
                contact.setNumber(no);
                contact.setCountryCode(countryCode);
                contactDao.addContact(contact, Delegate.MOBILE);
            }
            if (Delegate.TYPE_SPECIFIER.equals(Delegate.HOME)) {
                countryCode = numberSplit[0];
                areaCode = numberSplit[1];
                no = numberSplit[2];
                String[] noSplit = no.split("[x]");
                no = noSplit[0];
                extension = noSplit[1];
                contact.setCountryCode(countryCode);
                contact.setAreaCode(areaCode);
                contact.setNumber(no);
                contact.setHomeExtension(extension);
                contactDao.addContact(contact, Delegate.HOME);
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
