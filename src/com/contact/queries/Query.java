package com.contact.queries;

public class Query {

    public static final String INSERT_CONTACT = "INSERT INTO CONTACT (FIRST_NAME, LAST_NAME, ENTER_EMAIL_ADDRESS) " +
            "VALUES(?, ?, ?)";

    public static final String LAST_INSERT_ID = "SET @CONTACT_ID = LAST_INSERT_ID()";

    public static final String OFFICE_NUMBER = "INSERT IGNORE INTO PHONE (CONTACT_ID, TYPE_ID, OFFICE_EXT, NUM) " +
            "VALUES(@CONTACT_ID, ?, ?, ?)";

    public static final String MOBILE_NUMBER = "INSERT IGNORE INTO PHONE (CONTACT_ID, TYPE_ID, COUNTRY_CODE, NUM) " +
            "VALUES(@CONTACT_ID, ?, ?, ?)";

    public static final String HOME_NUMBER = "INSERT IGNORE INTO PHONE (CONTACT_ID, TYPE_ID, HOME_EXT, COUNTRY_CODE, AREA_CODE, NUM) " +
            "VALUES(@CONTACT_ID, ?, ?, ?, ?, ?)";

    public static final String CONTACT_ID_CON_1 = "SELECT CONTACT_ID, EMAIL_ADDRESS FROM CONTACT WHERE FIRST_NAME = ";

    public static final String CONTACT_ID_CON_2 = " AND LAST_NAME = ";

    public static final String ADD_OFFICE_NUMBER = "INSERT IGNORE INTO PHONE (CONTACT_ID, TYPE_ID, OFFICE_EXT, NUM) " +
            "VALUES(";

    public static final String VALUES = ", ?, ?, ?)";

    public static final String ADD_MOBILE_NUMBER = "INSERT IGNORE INTO PHONE (CONTACT_ID, TYPE_ID, COUNTRY_CODE, NUM) " +
            "VALUES(";

    public static final String ADD_HOME_NUMBER = "INSERT IGNORE INTO PHONE (CONTACT_ID, TYPE_ID, HOME_EXT, COUNTRY_CODE, AREA_CODE, NUM)" +
            "VALUES(";

    public static final String HOME_VALUES = ", ?, ?, ?, ?, ?)";

    public static final String VIEW_CONTACT = "SELECT * FROM PHONE WHERE CONTACT_ID = ";

    public static final String PHONE_TYPE = "SELECT * FROM PHONE_TYPE";

}
