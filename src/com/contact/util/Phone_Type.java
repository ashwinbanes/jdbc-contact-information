package com.contact.util;

import com.contact.queries.Query;

import java.sql.*;
import java.util.HashMap;

public class Phone_Type {

    private Connection connection = DBConnection.getConnection();
    private Statement statement;
    private ResultSet resultSet;
    public static final HashMap<String, Integer> phone_list = new HashMap<>();

    public void getPhoneType() {
       try {
           statement = connection.createStatement();
           resultSet = statement.executeQuery(Query.PHONE_TYPE);
           while (resultSet.next()) {
               phone_list.put(resultSet.getString(2), resultSet.getInt(1));
           }
       }catch (SQLException e) {
           e.printStackTrace();
       }finally {
           DBUtil.close(resultSet);
           DBUtil.close(statement);
           DBUtil.close(connection);
       }
    }

}
