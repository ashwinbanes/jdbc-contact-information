package com.contact.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;


public class DBConnection {

    private static final Logger LOGGER = Logger.getLogger(DBConnection.class.getName());

    private static Connection createConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(DBUtil.URL, DBUtil.USER_NAME,
                    DBUtil.PASSWORD);
            if(connection != null) {
                LOGGER.info(PrintStatements.DB_CONNECTION);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public static Connection getConnection() {
        return createConnection();
    }

}
