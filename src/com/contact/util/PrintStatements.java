package com.contact.util;


public class PrintStatements {

    public static final String CONTACT_CRUD = "Contact Details CRUD :)";

    public static final String MENU = "1. ADD, 2. UPDATE, 3. READ";

    public static final String FIRST_NAME = "Enter first name : ";

    public static final String LAST_NAME = "Enter Last name : ";

    public static final String ENTER_EMAIL_ADDRESS = "Enter email address : ";

    public static final String EMAIL_ADDRESS = "Email Address : ";

    public static final String VALID_EMAIL_ADDRESS = "Enter a valid email address : ";

    public static final String PHONE_NUMBER = "Enter phone number : ";

    public static final String VALID_PHONE_NUMBER = "Enter a valid phone number : ";

    public static final String CONTACT_ADDED = "New Contact added :)";

    public static final String ERROR = "An Error has occurred during DB operations :(";

    public static final String CONTACT_UPDATED = "The Contact has been successfully updated :)";

    public static final String DB_CONNECTION = "Database has been connected successfully :)";

    public static final String COUNTRY_CODE = "Country Code : ";

    public static final String AREA_CODE = "Area Code : ";

    public static final String OFFICE_EXTENSION = "Office Extension : ";

    public static final String NUMBER = "Number : ";

    public static final String HOME_EXTENSION = "Home Extension : ";

    public static final String SPACE = " ";

    public static final String NUMBER_FORMAT_HELPER = "Enter the number in your desired format : ";

    public static final String NUMBER_FORMAT_OFFICE = "<number>-<extension>";

    public static final String NUMBER_FORMAT_MOBILE = "<country_code>-<number>";

    public static final String NUMBER_FORMAT_HOME = "<country_code>-<area_code>-<number>-x<extension>";

    public static final String CONTINUE_EXIT_MENU = "Enter anything to continue and No to exit.";

}
