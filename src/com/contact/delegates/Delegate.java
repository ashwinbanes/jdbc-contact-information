package com.contact.delegates;

import com.contact.model.Contact;
import com.contact.util.Phone_Type;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Delegate {

    private static final Pattern VALID_EMAIL_REGEX = Pattern
            .compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
                    Pattern.CASE_INSENSITIVE);

    private static final Pattern MOBILE_NUMBER_VALIDATION = Pattern
            .compile("^\\+[0-9]{1,3}\\-[0-9]{10,14}$", Pattern.CASE_INSENSITIVE);

    private static final Pattern OFFICE_NUMBER_VALIDATION = Pattern
            .compile("^[0-9]{10,14}(?:x.+)?$", Pattern.CASE_INSENSITIVE);

    private static final Pattern HOME_NUMBER_VALIDATION = Pattern
            .compile("^\\+[0-9]{1,3}\\-([0-9]{1,3})\\-[0-9]{4,14}(?:x.+)?$", Pattern.CASE_INSENSITIVE);

    public static String TYPE_SPECIFIER = "";

    public static final String OFFICE = "OFFICE";

    public static final String MOBILE = "MOBILE";

    public static final String HOME = "HOME";

    private static Matcher matcher;

    public static boolean emailValidator(String email) {
        matcher = VALID_EMAIL_REGEX.matcher(email);
        return matcher.find();
    }

    public static boolean numberValidator(String number) {

        if (OFFICE_NUMBER_VALIDATION.matcher(number).find()) {
            TYPE_SPECIFIER = "OFFICE";
            return true;
        }
        if (MOBILE_NUMBER_VALIDATION.matcher(number).find()) {
            TYPE_SPECIFIER = "MOBILE";
            return true;
        }
        if (HOME_NUMBER_VALIDATION.matcher(number).find()) {
            TYPE_SPECIFIER = "HOME";
            return true;
        }
        return false;
    }

    public static String addQuotes(String value) {
        return "\'" + value + "\'";
    }

    public static PreparedStatement prepareStatementOffice(PreparedStatement preparedStatement, Contact contact) throws SQLException {
        preparedStatement.setInt(1, Phone_Type.phone_list.get(Delegate.OFFICE));
        preparedStatement.setString(2, contact.getOfficeExtension());
        preparedStatement.setString(3, contact.getNumber());
        return preparedStatement;
    }

    public static PreparedStatement preparedStatementMobile(PreparedStatement preparedStatement, Contact contact) throws SQLException {
        preparedStatement.setInt(1, Phone_Type.phone_list.get(Delegate.MOBILE));
        preparedStatement.setString(2, contact.getCountryCode());
        preparedStatement.setString(3, contact.getNumber());
        return preparedStatement;
    }

    public static PreparedStatement preparedStatementHome(PreparedStatement preparedStatement, Contact contact) throws SQLException {
        preparedStatement.setInt(1, Phone_Type.phone_list.get(Delegate.HOME));
        preparedStatement.setString(2, contact.getHomeExtension());
        preparedStatement.setString(3, contact.getCountryCode());
        preparedStatement.setString(4, contact.getAreaCode());
        preparedStatement.setString(5, contact.getNumber());
        return preparedStatement;
    }

    public static <K, V> K getKey(Map<K, V> map, V value) {
        for (K key : map.keySet()) {
            if (value.equals(map.get(key))) {
                return key;
            }
        }
        return null;
    }

//    public static void viewSet(Contact contact, String first_name, String last_name, String email) {
//
//    }
//
//    public static void viewSet(Contact contact, String no, String country_code) {
//
//    }
//
//    public static void viewSet(Contact contact, String country_code, String area_code, String no, String extension) {
//
//    }

}
