package com.contact.dao;

import com.contact.delegates.Delegate;
import com.contact.model.Contact;
import com.contact.queries.Query;
import com.contact.util.DBConnection;
import com.contact.util.DBUtil;
import com.contact.util.Phone_Type;
import com.contact.util.PrintStatements;
import com.contact.view.Disp;

import java.sql.*;

public class ContactDao {

    private Connection connection = DBConnection.getConnection();
    private Statement statement;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet;

    public ContactDao() {
    }

    public void addContact(Contact contact, String type) {

        try {
            statement = connection.createStatement();
            preparedStatement = connection.prepareStatement(Query.INSERT_CONTACT);
            preparedStatement.setString(1, contact.getFirstName());
            preparedStatement.setString(2, contact.getLastName());
            preparedStatement.setString(3, contact.getEmail());
            int update_1 = preparedStatement.executeUpdate();
            if (type.equals(Delegate.OFFICE)) {
                if (update_1 > 0) {
                    statement.execute(Query.LAST_INSERT_ID);
                    preparedStatement = connection.prepareStatement(Query.OFFICE_NUMBER);
                    int update_2 = Delegate.prepareStatementOffice(preparedStatement, contact).executeUpdate();
                    if (update_2 > 0) {
                        Disp.print(PrintStatements.CONTACT_ADDED);
                    }
                }else {
                    Disp.print(PrintStatements.ERROR);
                }
            }
            if (type.equals(Delegate.MOBILE)) {
                if (update_1 > 0) {
                    statement.execute(Query.LAST_INSERT_ID);
                    preparedStatement = connection.prepareStatement(Query.MOBILE_NUMBER);
                    int update_2 = Delegate.preparedStatementMobile(preparedStatement, contact).executeUpdate();
                    if (update_2 > 0) {
                        Disp.print(PrintStatements.CONTACT_ADDED);
                    }
                }else {
                    Disp.print(PrintStatements.ERROR);
                }
            }
            if (type.equals(Delegate.HOME)) {
                if (update_1 > 0) {
                    statement.execute(Query.LAST_INSERT_ID);
                    preparedStatement = connection.prepareStatement(Query.HOME_NUMBER);
                    int update_2 = Delegate.preparedStatementHome(preparedStatement, contact).executeUpdate();
                    if (update_2 > 0) {
                        Disp.print(PrintStatements.CONTACT_ADDED);
                    }
                }else {
                    Disp.print(PrintStatements.ERROR);
                }
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(statement);
            DBUtil.close(connection);
        }
    }


    public void updateContact(Contact contact, String type) {
        try {
            statement = connection.createStatement();
            Disp.print(Query.CONTACT_ID_CON_1 + Delegate.addQuotes(contact.getFirstName()) + Query.CONTACT_ID_CON_2 +
                    Delegate.addQuotes(contact.getLastName()));
            if (type.equals(Delegate.OFFICE)) {
                resultSet = statement.executeQuery(Query.CONTACT_ID_CON_1 + Delegate.addQuotes(contact.getFirstName()) + Query.CONTACT_ID_CON_2 +
                        Delegate.addQuotes(contact.getLastName()));
                while (resultSet.next()) {
                    int contact_id = resultSet.getInt(1);

                    if (contact_id > 0) {
                        preparedStatement = connection.prepareStatement(Query.ADD_OFFICE_NUMBER + contact_id
                                + Query.VALUES);
                        Delegate.prepareStatementOffice(preparedStatement, contact).executeUpdate();
                        Disp.print(PrintStatements.CONTACT_UPDATED);
                    }else {
                        Disp.print(PrintStatements.ERROR);
                    }

                }
            }

            if (type.equals(Delegate.MOBILE)) {
                resultSet = statement.executeQuery(Query.CONTACT_ID_CON_1 + Delegate.addQuotes(contact.getFirstName()) + Query.CONTACT_ID_CON_2 +
                        Delegate.addQuotes(contact.getLastName()));
                while (resultSet.next()) {
                    int contact_id = resultSet.getInt(1);

                    if (contact_id > 0) {
                        preparedStatement = connection.prepareStatement(Query.ADD_MOBILE_NUMBER + contact_id
                                + Query.VALUES);
                        Delegate.preparedStatementMobile(preparedStatement, contact).executeUpdate();
                        Disp.print(PrintStatements.CONTACT_UPDATED);
                    }else {
                        Disp.print(PrintStatements.ERROR);
                    }

                }
            }

            if (type.equals(Delegate.HOME)) {
                resultSet = statement.executeQuery(Query.CONTACT_ID_CON_1 + Delegate.addQuotes(contact.getFirstName()) + Query.CONTACT_ID_CON_2 +
                        Delegate.addQuotes(contact.getLastName()));
                while (resultSet.next()) {
                    int contact_id = resultSet.getInt(1);

                    if (contact_id > 0) {
                        preparedStatement = connection.prepareStatement(Query.ADD_HOME_NUMBER + contact_id
                                + Query.HOME_VALUES);
                        Delegate.preparedStatementHome(preparedStatement, contact).executeUpdate();
                        Disp.print(PrintStatements.CONTACT_UPDATED);
                    }else {
                        Disp.print(PrintStatements.ERROR);
                    }

                }
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(resultSet);
            DBUtil.close(statement);
            DBUtil.close(connection);
        }
    }


    public void viewContact(Contact contact) {

        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(Query.CONTACT_ID_CON_1 + contact.getFirstName() + Query.CONTACT_ID_CON_2 +
                    contact.getLastName());
            while (resultSet.next()) {
                int contact_id = resultSet.getInt(1);
                Disp.printLine(PrintStatements.EMAIL_ADDRESS + PrintStatements.SPACE +
                        resultSet.getString(2));
                System.out.println();
                if (contact_id > 0) {
                    resultSet = statement.executeQuery(Query.VIEW_CONTACT + contact_id);
                    while (resultSet.next()) {

                        String type = Delegate.getKey(Phone_Type.phone_list, resultSet.getInt(3));

                        if (type.equals(Delegate.OFFICE)) {
                            Disp.printLine(type + PrintStatements.SPACE +
                                    resultSet.getInt(8) + "x" +
                                    resultSet.getInt(4));
                            System.out.println();
                            Disp.printLine(PrintStatements.OFFICE_EXTENSION
                                    + resultSet.getInt(4) + PrintStatements.SPACE);
                            Disp.printLine(PrintStatements.NUMBER
                                    + resultSet.getInt(8) + PrintStatements.SPACE);
                            System.out.println();
                        }

                        if (type.equals(Delegate.MOBILE)) {
                            Disp.printLine(type + PrintStatements.SPACE +
                                    resultSet.getInt(6) + "-" +
                                    resultSet.getInt(8));
                            System.out.println();
                            Disp.printLine(PrintStatements.COUNTRY_CODE
                                    + resultSet.getInt(6) + PrintStatements.SPACE);
                            Disp.printLine(PrintStatements.NUMBER
                                    + resultSet.getInt(8) + PrintStatements.SPACE);
                            System.out.println();
                        }

                        if (type.equals(Delegate.HOME)) {
                            Disp.printLine(type + PrintStatements.SPACE +
                                    resultSet.getInt(6) + "-" +
                                    resultSet.getInt(7) + "-" +
                                    resultSet.getInt(8) + "x" +
                                    resultSet.getInt(5));
                            System.out.println();
                            Disp.printLine(PrintStatements.HOME_EXTENSION
                                    + resultSet.getInt(5) + PrintStatements.SPACE);
                            Disp.printLine(PrintStatements.COUNTRY_CODE
                                    + resultSet.getInt(6) + PrintStatements.SPACE);
                            Disp.printLine(PrintStatements.AREA_CODE
                                    + resultSet.getInt(7) + PrintStatements.SPACE);
                            Disp.printLine(PrintStatements.NUMBER
                                    + resultSet.getInt(8) + PrintStatements.SPACE);
                            System.out.println();
                        }
                    }
                }
            }
        }catch (SQLException e) {
            DBUtil.close(resultSet);
            DBUtil.close(statement);
            DBUtil.close(connection);
        }

    }
}
